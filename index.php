<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сервис коротких ссылок");
?>

<div class="app-logo">
	<div class="link app-logo__icon" target="_blank">
		<img alt="🔗" aria-label="🔗" src="/local/templates/furniture_dark-blue/images/1f517.svg" style="width: 78px; height: 78px;">
	</div>
	<h1 class="app-logo__text">Сервис коротких ссылок</h1>
</div>

<?php
    require_once('mysql.php');
    if(isset($_POST['link'])) {
        $link = $_POST['link'];
        if(!empty($link)) {
            $link = mysqli_real_escape_string($conn, $link);
            if(@$select = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * FROM `short` WHERE `url` = '".$link."'"))) {
                $result = [
                    'url'  => $select['url'],
                    'key'  => $select['short_key'],
                    'link' => 'http://'.$_SERVER['HTTP_HOST'].'/-'.$select['short_key']
                ];
            }
            else {
                $letters='qwertyuiopasdfghjklzxcvbnm1234567890';
                $count=strlen($letters);
                $intval=time();
                $result='';
                for($i=0;$i<4;$i++) {
                    $last=$intval%$count;
                    $intval=($intval-$last)/$count;
                    $result.=$letters[$last];
                }
                mysqli_query($conn, "INSERT INTO `short` (`id`, `url`, `short_key`) VALUES (NULL, '".$link."', '".$result.$intval."') ");
                @$select = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * FROM `short` WHERE `url` = '".$link."'"));
                
                $result = [
                    'url'  => $select['url'],
                    'key'  => $select['short_key'],
                    'link' => 'http://'.$_SERVER['HTTP_HOST'].'/-'.$select['short_key']
                ];
               
            }
        }
    } 
?>

<form method="post" action="">
	<div class="shortener__url-input-container">
		<div class="text-input shortener__url-input text-input_view_contrast text-input_size_s56">
			<span class="text-input__content">
				<input class="text-input__box" type="text" name="link" pattern="https?://.+" placeholder="Введите ссылку, которую нужно сократить" >
				</span>
			</span>
		</div>
		<div class="button__shape">
			<div class="button__content">
				<input type="submit" name="Сократить" value="Сократить">
			</div>
		</div>
	</div>
</form>

<?if ('' != $result['link']){?>
	<a target="_blank" class="button_content_link" href="<?php echo $result['link']; ?>" ><?php echo $result['link']; ?></a>
<?}?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>